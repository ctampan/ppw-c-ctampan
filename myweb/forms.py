from django import forms


class PostForm(forms.Form):
    error_message = {
        'required' : 'Tolong isi input ini',
        'invalid' : 'Isi input dengan alamat email',
        }

    activity = forms.CharField(label='Activity', required=False, max_length=27,
                            widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Activity'}))
    location = forms.CharField(label='Location', required = False,max_length=25, widget = forms.TextInput(attrs={'class':'form-control', 'placeholder':'Location'}))
    start_date = forms.DateTimeField(label='Start Date', required=False, widget=forms.DateInput(attrs={'type':'date','class' : 'form-control', 'value':'1990-01-01'}))
    start_time = forms.DateTimeField(label='Start Time', required=False, widget=forms.TimeInput(attrs={'type':'time','class' : 'form-control'}))
    end_date = forms.DateTimeField(label='End Date', required=False, widget=forms.DateInput(attrs={'type':'date','class' : 'form-control', 'value':'1990-01-01'}))
    end_time = forms.DateTimeField(label='End Time', required=False, widget=forms.TimeInput(attrs={'type':'time','class' : 'form-control'}))
    notes = forms.CharField(widget=forms.Textarea(attrs={'class':'form-control'}),required=True)
    #empty_Value='Anonymous',
