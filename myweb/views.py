from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import PostForm
from .models import Message

# Create your views here.
response = {}
def index(request) :
    return render(request, 'index.html')

def contact(request) :
    return render(request, 'contact.html')

def education(request) :
    return render(request, 'education.html')

def experience(request) :
    return render(request, 'experience.html')

def profile(request) :
    return render(request, 'profile.html')

def form(request):
    response['message_form']=PostForm
    return render(request, 'form.html', response)

def action(request):
    if request.method == "POST":
        response['activity']=request.POST['activity']
        response['location']=request.POST['location']
        response['start_date']=request.POST['start_date']
        response['start_time']=request.POST['start_time']
        response['end_date']=request.POST['end_date']
        response['end_time']=request.POST['end_time']
        response['notes']=request.POST['notes']
        project = Message(activity = response['activity'], location = response['location'],
                          start_date = response['start_date'], start_time = response['start_time'],
                          end_date = response['end_date'], end_time = response['end_time'],
                          notes = response['notes'])
        project.save()
        return HttpResponseRedirect('list')
    else:
        return HttpResponseRedirect('list')

def list(request):
    data = Message.objects.all()
    response['data'] = data
    return render(request, 'form_list.html', response)

def clear(request):
    data = Message.objects.all()
    data.delete()
    response['data'] = data
    #return HttpResponseRedirect('list')
    return HttpResponseRedirect('list')
