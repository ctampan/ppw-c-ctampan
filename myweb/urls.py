from django.urls import path
from myweb import views

urlpatterns = [
    path("", views.index, name="index"),
    path("education", views.education, name="education"),
    path("experience", views.experience, name="experience"),
    path("profile", views.profile, name="profile"),
    path("contact/", views.contact, name="contact"),
    path("form", views.form, name="form"),
    path("process", views.action, name="process"),
    path("list", views.list, name="list"),
    path("clear", views.clear, name="clear")
    ]
